-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  mar. 09 juin 2020 à 07:52
-- Version du serveur :  5.7.19
-- Version de PHP :  7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mailfim`
--
CREATE DATABASE IF NOT EXISTS `mailfim` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mailfim`;

-- --------------------------------------------------------

--
-- Structure de la table `campaign`
--

CREATE TABLE `campaign` (
  `id_campaign` int(11) NOT NULL,
  `name_campaign` varchar(100) NOT NULL,
  `object_campaign` text NOT NULL,
  `FK_id_template` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `campaign_list`
--

CREATE TABLE `campaign_list` (
  `id_campaign_list` int(11) NOT NULL,
  `FK_id_campaign` int(11) NOT NULL,
  `FK_id_list` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(11) NOT NULL,
  `lastname_contact` varchar(50) NOT NULL,
  `firstname_contact` varchar(50) NOT NULL,
  `mail_contact` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `contact_list`
--

CREATE TABLE `contact_list` (
  `id_contact_list` int(11) NOT NULL,
  `FK_id_contact` int(11) NOT NULL,
  `FK_id_list` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id_image` int(11) NOT NULL,
  `name_image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE `liste` (
  `id_list` int(11) NOT NULL,
  `name_list` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reporting`
--

CREATE TABLE `reporting` (
  `id_reporting` int(11) NOT NULL,
  `open_reporting` int(11) NOT NULL,
  `click_reporting` int(11) NOT NULL,
  `unsubscribe_reporting` int(11) NOT NULL,
  `spam_reporting` int(11) NOT NULL,
  `reception_reporting` int(11) NOT NULL,
  `FK_id_campaign` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id_template` int(11) NOT NULL,
  `name_template` varchar(100) NOT NULL,
  `file_template` json NOT NULL,
  `FK_id_image` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `mail_user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id_campaign`),
  ADD KEY `FK_id_template` (`FK_id_template`);

--
-- Index pour la table `campaign_list`
--
ALTER TABLE `campaign_list`
  ADD PRIMARY KEY (`id_campaign_list`),
  ADD KEY `FK_id_campaign` (`FK_id_campaign`),
  ADD KEY `FK_id_list` (`FK_id_list`) USING BTREE;

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Index pour la table `contact_list`
--
ALTER TABLE `contact_list`
  ADD PRIMARY KEY (`id_contact_list`),
  ADD KEY `FK_id_list` (`FK_id_contact`),
  ADD KEY `FK_id_contact` (`FK_id_contact`),
  ADD KEY `FK_id_list_2` (`FK_id_list`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id_image`);

--
-- Index pour la table `liste`
--
ALTER TABLE `liste`
  ADD PRIMARY KEY (`id_list`);

--
-- Index pour la table `reporting`
--
ALTER TABLE `reporting`
  ADD PRIMARY KEY (`id_reporting`),
  ADD KEY `FK_id_campaign` (`FK_id_campaign`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id_template`),
  ADD KEY `FK_id_image` (`FK_id_image`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id_campaign` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `campaign_list`
--
ALTER TABLE `campaign_list`
  MODIFY `id_campaign_list` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `contact_list`
--
ALTER TABLE `contact_list`
  MODIFY `id_contact_list` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id_image` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reporting`
--
ALTER TABLE `reporting`
  MODIFY `id_reporting` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `campaign`
--
ALTER TABLE `campaign`
  ADD CONSTRAINT `campaign_ibfk_1` FOREIGN KEY (`FK_id_template`) REFERENCES `template` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `campaign_list`
--
ALTER TABLE `campaign_list`
  ADD CONSTRAINT `campaign_list_ibfk_1` FOREIGN KEY (`FK_id_campaign`) REFERENCES `campaign` (`id_campaign`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_list_ibfk_2` FOREIGN KEY (`FK_id_list`) REFERENCES `liste` (`id_list`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `contact_list`
--
ALTER TABLE `contact_list`
  ADD CONSTRAINT `contact_list_ibfk_1` FOREIGN KEY (`FK_id_contact`) REFERENCES `contact` (`id_contact`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contact_list_ibfk_2` FOREIGN KEY (`FK_id_list`) REFERENCES `liste` (`id_list`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reporting`
--
ALTER TABLE `reporting`
  ADD CONSTRAINT `reporting_ibfk_1` FOREIGN KEY (`FK_id_campaign`) REFERENCES `campaign` (`id_campaign`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `template`
--
ALTER TABLE `template`
  ADD CONSTRAINT `template_ibfk_1` FOREIGN KEY (`FK_id_image`) REFERENCES `image` (`id_image`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
