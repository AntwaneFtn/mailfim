<?php

namespace App\Form;

use App\Entity\Liste;
use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname_contact', TextType::class)
            ->add('firstname_contact', TextType::class)
            ->add('mail_contact', TextType::class)
            ->add('FK_id_liste', EntityType::class, [
                // looks for choices from this entity
                'class' => Liste::class,

                // uses the User.username property as the visible option string
                'choice_label' => 'name_liste',
                
                'label' => "Ajouter à une ou plusieurs listes :",

                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
