<?php

namespace App\Form;

use App\Entity\Liste;
use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ListeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_liste')
            // ->add('FK_id_contact', CollectionType::class, [
            //     // each entry in the array will be an "email" field
            //     'entry_type' => ContactType::class,
            //     'entry_options' => array('label' => false),
            //     'allow_add' => true,
            // ])
            ->add('FK_id_contact', EntityType::class, [
                // looks for choices from this entity
                'class' => Contact::class,
                'label' => "Ajouter des contacts :",

                // uses the User.username property as the visible option string
                'choice_label' => 'fullname',

                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => true,
            ])
            //->add('FK_id_campaign')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Liste::class,
        ]);
    }
}
