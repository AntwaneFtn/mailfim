<?php

namespace App\Form;

use App\Entity\Liste;
use App\Entity\Campaign;
use App\Entity\Template;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampaignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_campaign', null, [
                'label' => "Nom de la campagne :",
            ])
            ->add('object_campaign', null, [
                'label' => "Objet de la campagne :",
            ])
            ->add('FK_id_template', EntityType::class, [
                'class' => Template::class,
                'choice_label' => 'name_template',
                'label' => "Ajouter un template :",
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('FK_id_liste', EntityType::class, [
                'class' => Liste::class,
                'choice_label' => 'name_liste',
                'label' => "Ajouter une ou plusieurs listes de contact :",
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Campaign::class,
        ]);
    }
}
