<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TemplateRepository")
 */
class Template
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name_template;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_template;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Image")
     */
    private $FK_id_image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Campaign", mappedBy="FK_id_template")
     */
    private $FK_id_campaign;

    public function __construct()
    {
        $this->FK_id_image = new ArrayCollection();
        $this->FK_id_campaign = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTemplate(): ?string
    {
        return $this->name_template;
    }

    public function setNameTemplate(string $name_template): self
    {
        $this->name_template = $name_template;

        return $this;
    }

    public function getFileTemplate(): ?string
    {
        return $this->file_template;
    }

    public function setFileTemplate(?string $file_template): self
    {
        $this->file_template = $file_template;

        return $this;
    }

    /**
     * @return Collection|image[]
     */
    public function getFKIdImage(): Collection
    {
        return $this->FK_id_image;
    }

    public function addFKIdImage(image $fKIdImage): self
    {
        if (!$this->FK_id_image->contains($fKIdImage)) {
            $this->FK_id_image[] = $fKIdImage;
        }

        return $this;
    }

    public function removeFKIdImage(image $fKIdImage): self
    {
        if ($this->FK_id_image->contains($fKIdImage)) {
            $this->FK_id_image->removeElement($fKIdImage);
        }

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getFKIdCampaign(): Collection
    {
        return $this->FK_id_campaign;
    }

    public function addFKIdCampaign(Campaign $fKIdCampaign): self
    {
        if (!$this->FK_id_campaign->contains($fKIdCampaign)) {
            $this->FK_id_campaign[] = $fKIdCampaign;
            $fKIdCampaign->addFKIdTemplate($this);
        }

        return $this;
    }

    public function removeFKIdCampaign(Campaign $fKIdCampaign): self
    {
        if ($this->FK_id_campaign->contains($fKIdCampaign)) {
            $this->FK_id_campaign->removeElement($fKIdCampaign);
            $fKIdCampaign->removeFKIdTemplate($this);
        }

        return $this;
    }
}
