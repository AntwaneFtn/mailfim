<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReportingRepository")
 */
class Reporting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $open_reporting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $click_reporting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unsubscribe_reporting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spam_reporting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reception_reporting;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Campaign", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $FK_id_campaign;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOpenReporting(): ?int
    {
        return $this->open_reporting;
    }

    public function setOpenReporting(?int $open_reporting): self
    {
        $this->open_reporting = $open_reporting;

        return $this;
    }

    public function getClickReporting(): ?int
    {
        return $this->click_reporting;
    }

    public function setClickReporting(?int $click_reporting): self
    {
        $this->click_reporting = $click_reporting;

        return $this;
    }

    public function getUnsubscribeReporting(): ?int
    {
        return $this->unsubscribe_reporting;
    }

    public function setUnsubscribeReporting(?int $unsubscribe_reporting): self
    {
        $this->unsubscribe_reporting = $unsubscribe_reporting;

        return $this;
    }

    public function getSpamReporting(): ?int
    {
        return $this->spam_reporting;
    }

    public function setSpamReporting(?int $spam_reporting): self
    {
        $this->spam_reporting = $spam_reporting;

        return $this;
    }

    public function getReceptionReporting(): ?int
    {
        return $this->reception_reporting;
    }

    public function setReceptionReporting(?int $reception_reporting): self
    {
        $this->reception_reporting = $reception_reporting;

        return $this;
    }

    public function getFKIdCampaign(): ?campaign
    {
        return $this->FK_id_campaign;
    }

    public function setFKIdCampaign(campaign $FK_id_campaign): self
    {
        $this->FK_id_campaign = $FK_id_campaign;

        return $this;
    }
}
