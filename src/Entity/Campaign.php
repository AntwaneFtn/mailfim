<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_campaign;

    /**
     * @ORM\Column(type="string", length=10000, nullable=true)
     */
    private $object_campaign;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Template", inversedBy="FK_id_campaign")
     */
    private $FK_id_template;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Liste", inversedBy="FK_id_campaign")
     */
    private $FK_id_liste;

    public function __construct()
    {
        $this->FK_id_template = new ArrayCollection();
        $this->FK_id_liste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCampaign(): ?string
    {
        return $this->name_campaign;
    }

    public function setNameCampaign(string $name_campaign): self
    {
        $this->name_campaign = $name_campaign;

        return $this;
    }

    public function getObjectCampaign(): ?string
    {
        return $this->object_campaign;
    }

    public function setObjectCampaign(?string $object_campaign): self
    {
        $this->object_campaign = $object_campaign;

        return $this;
    }

    /**
     * @return Collection|template[]
     */
    public function getFKIdTemplate(): Collection
    {
        return $this->FK_id_template;
    }

    public function addFKIdTemplate(template $fKIdTemplate): self
    {
        if (!$this->FK_id_template->contains($fKIdTemplate)) {
            $this->FK_id_template[] = $fKIdTemplate;
        }

        return $this;
    }

    public function removeFKIdTemplate(template $fKIdTemplate): self
    {
        if ($this->FK_id_template->contains($fKIdTemplate)) {
            $this->FK_id_template->removeElement($fKIdTemplate);
        }

        return $this;
    }

    /**
     * @return Collection|liste[]
     */
    public function getFKIdListe(): Collection
    {
        return $this->FK_id_liste;
    }

    public function addFKIdListe(liste $fKIdListe): self
    {
        if (!$this->FK_id_liste->contains($fKIdListe)) {
            $this->FK_id_liste[] = $fKIdListe;
        }

        return $this;
    }

    public function removeFKIdListe(liste $fKIdListe): self
    {
        if ($this->FK_id_liste->contains($fKIdListe)) {
            $this->FK_id_liste->removeElement($fKIdListe);
        }

        return $this;
    }
}
