<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $lastname_contact;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $firstname_contact;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $mail_contact;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Liste", mappedBy="FK_id_contact")
     */
    private $FK_id_liste;

    public function __construct()
    {
        $this->FK_id_liste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName()
    {
        return $this->lastname_contact.' '.$this->firstname_contact;
    }

    public function getLastnameContact(): ?string
    {
        return $this->lastname_contact;
    }

    public function setLastnameContact(string $lastname_contact): self
    {
        $this->lastname_contact = $lastname_contact;

        return $this;
    }

    public function getFirstnameContact(): ?string
    {
        return $this->firstname_contact;
    }

    public function setFirstnameContact(string $firstname_contact): self
    {
        $this->firstname_contact = $firstname_contact;

        return $this;
    }

    public function getMailContact(): ?string
    {
        return $this->mail_contact;
    }

    public function setMailContact(string $mail_contact): self
    {
        $this->mail_contact = $mail_contact;

        return $this;
    }

    /**
     * @return Collection|Liste[]
     */
    public function getFKIdListe(): Collection
    {
        return $this->FK_id_liste;
    }

    public function addFKIdListe(Liste $fKIdListe): self
    {
        if (!$this->FK_id_liste->contains($fKIdListe)) {
            $this->FK_id_liste[] = $fKIdListe;
            $fKIdListe->addFKIdContact($this);
        }

        return $this;
    }

    public function removeFKIdListe(Liste $fKIdListe): self
    {
        if ($this->FK_id_liste->contains($fKIdListe)) {
            $this->FK_id_liste->removeElement($fKIdListe);
            $fKIdListe->removeFKIdContact($this);
        }

        return $this;
    }
}
