<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListeRepository")
 */
class Liste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name_liste;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contact", inversedBy="FK_id_liste")
     */
    private $FK_id_contact;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Campaign", mappedBy="FK_id_liste")
     */
    private $FK_id_campaign;

    public function __construct()
    {
        $this->FK_id_contact = new ArrayCollection();
        $this->FK_id_campaign = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameListe(): ?string
    {
        return $this->name_liste;
    }

    public function setNameListe(string $name_liste): self
    {
        $this->name_liste = $name_liste;

        return $this;
    }

    /**
     * @return Collection|contact[]
     */
    public function getFKIdContact(): Collection
    {
        return $this->FK_id_contact;
    }

    public function addFKIdContact(contact $fKIdContact): self
    {
        if (!$this->FK_id_contact->contains($fKIdContact)) {
            $this->FK_id_contact[] = $fKIdContact;
        }

        return $this;
    }

    public function removeFKIdContact(contact $fKIdContact): self
    {
        if ($this->FK_id_contact->contains($fKIdContact)) {
            $this->FK_id_contact->removeElement($fKIdContact);
        }

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getFKIdCampaign(): Collection
    {
        return $this->FK_id_campaign;
    }

    public function addFKIdCampaign(Campaign $fKIdCampaign): self
    {
        if (!$this->FK_id_campaign->contains($fKIdCampaign)) {
            $this->FK_id_campaign[] = $fKIdCampaign;
            $fKIdCampaign->addFKIdListe($this);
        }

        return $this;
    }

    public function removeFKIdCampaign(Campaign $fKIdCampaign): self
    {
        if ($this->FK_id_campaign->contains($fKIdCampaign)) {
            $this->FK_id_campaign->removeElement($fKIdCampaign);
            $fKIdCampaign->removeFKIdListe($this);
        }

        return $this;
    }
}
