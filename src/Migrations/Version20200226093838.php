<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226093838 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE campaign (id INT AUTO_INCREMENT NOT NULL, name_campaign VARCHAR(255) NOT NULL, object_campaign VARCHAR(10000) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign_template (campaign_id INT NOT NULL, template_id INT NOT NULL, INDEX IDX_A510C9FCF639F774 (campaign_id), INDEX IDX_A510C9FC5DA0FB8 (template_id), PRIMARY KEY(campaign_id, template_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campaign_liste (campaign_id INT NOT NULL, liste_id INT NOT NULL, INDEX IDX_B2F74E9BF639F774 (campaign_id), INDEX IDX_B2F74E9BE85441D8 (liste_id), PRIMARY KEY(campaign_id, liste_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, lastname_contact VARCHAR(150) NOT NULL, firstname_contact VARCHAR(150) NOT NULL, mail_contact VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, name_image VARCHAR(300) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste (id INT AUTO_INCREMENT NOT NULL, name_liste VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_contact (liste_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_FD587E69E85441D8 (liste_id), INDEX IDX_FD587E69E7A1254A (contact_id), PRIMARY KEY(liste_id, contact_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reporting (id INT AUTO_INCREMENT NOT NULL, fk_id_campaign_id INT NOT NULL, open_reporting INT DEFAULT NULL, click_reporting INT DEFAULT NULL, unsubscribe_reporting INT DEFAULT NULL, spam_reporting INT DEFAULT NULL, reception_reporting INT DEFAULT NULL, UNIQUE INDEX UNIQ_BD7CFA9F9B25D36A (fk_id_campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE template (id INT AUTO_INCREMENT NOT NULL, name_template VARCHAR(100) NOT NULL, file_template JSON DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE template_image (template_id INT NOT NULL, image_id INT NOT NULL, INDEX IDX_F6EBD7905DA0FB8 (template_id), INDEX IDX_F6EBD7903DA5256D (image_id), PRIMARY KEY(template_id, image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(50) NOT NULL, password VARCHAR(1000) NOT NULL, mail_user VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE campaign_template ADD CONSTRAINT FK_A510C9FCF639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE campaign_template ADD CONSTRAINT FK_A510C9FC5DA0FB8 FOREIGN KEY (template_id) REFERENCES template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE campaign_liste ADD CONSTRAINT FK_B2F74E9BF639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE campaign_liste ADD CONSTRAINT FK_B2F74E9BE85441D8 FOREIGN KEY (liste_id) REFERENCES liste (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_contact ADD CONSTRAINT FK_FD587E69E85441D8 FOREIGN KEY (liste_id) REFERENCES liste (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_contact ADD CONSTRAINT FK_FD587E69E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reporting ADD CONSTRAINT FK_BD7CFA9F9B25D36A FOREIGN KEY (fk_id_campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE template_image ADD CONSTRAINT FK_F6EBD7905DA0FB8 FOREIGN KEY (template_id) REFERENCES template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE template_image ADD CONSTRAINT FK_F6EBD7903DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaign_template DROP FOREIGN KEY FK_A510C9FCF639F774');
        $this->addSql('ALTER TABLE campaign_liste DROP FOREIGN KEY FK_B2F74E9BF639F774');
        $this->addSql('ALTER TABLE reporting DROP FOREIGN KEY FK_BD7CFA9F9B25D36A');
        $this->addSql('ALTER TABLE liste_contact DROP FOREIGN KEY FK_FD587E69E7A1254A');
        $this->addSql('ALTER TABLE template_image DROP FOREIGN KEY FK_F6EBD7903DA5256D');
        $this->addSql('ALTER TABLE campaign_liste DROP FOREIGN KEY FK_B2F74E9BE85441D8');
        $this->addSql('ALTER TABLE liste_contact DROP FOREIGN KEY FK_FD587E69E85441D8');
        $this->addSql('ALTER TABLE campaign_template DROP FOREIGN KEY FK_A510C9FC5DA0FB8');
        $this->addSql('ALTER TABLE template_image DROP FOREIGN KEY FK_F6EBD7905DA0FB8');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE campaign_template');
        $this->addSql('DROP TABLE campaign_liste');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE liste');
        $this->addSql('DROP TABLE liste_contact');
        $this->addSql('DROP TABLE reporting');
        $this->addSql('DROP TABLE template');
        $this->addSql('DROP TABLE template_image');
        $this->addSql('DROP TABLE user');
    }
}
