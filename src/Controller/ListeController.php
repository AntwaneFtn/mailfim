<?php

namespace App\Controller;

use App\Entity\Liste;
use App\Form\ListeType;
use App\Repository\ListeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/liste")
 */
class ListeController extends AbstractController
{
    /**
     * @Route("/", name="liste_index", methods={"GET"})
     */
    public function index(ListeRepository $listeRepository): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $listeData = $listeRepository->findAll();

        $jsonObject = $serializer->serialize($listeData, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return $this->render('liste/index.html.twig', [
            'listes' => $jsonObject,
            'current' => 'Liste',
        ]);
    }

    /**
     * @Route("/new", name="liste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $liste = new Liste();
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($liste);
            $entityManager->flush();

            return $this->redirectToRoute('liste_index');
        }

        return $this->render('liste/new.html.twig', [
            'liste' => $liste,
            'form' => $form->createView(),
            'current' => 'Liste',
        ]);
    }

    /**
     * @Route("/{id}", name="liste_show", methods={"GET"})
     */
    public function show(Liste $liste): Response
    {
        return $this->render('liste/show.html.twig', [
            'liste' => $liste,
            'current' => 'Liste',
        ]);
    }

    /**
     * @Route("/{id}/edit", name="liste_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Liste $liste): Response
    {
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste_index');
        }

        return $this->render('liste/edit.html.twig', [
            'liste' => $liste,
            'form' => $form->createView(),
            'current' => 'Liste',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="liste_delete", methods={"POST"})
     */
    public function delete(Request $request, Liste $liste): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($liste);
            $entityManager->flush();
        
        return $this->redirectToRoute('liste_index');
    }
}
