export default class Campagne {

    static getFields () {
        let fields = [
            { key: 'nameCampaign', label: 'Nom', sortable: true, sortDirection: 'desc' },
            { key: 'objectCampaign', label: 'Objet', sortable: true, sortDirection: 'desc' },
            { key: 'actions', label: 'Actions', sortable: false }
        ]
        return fields
    }
}
