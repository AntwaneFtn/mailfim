export default class Template {

    static getFields () {
        let fields = [
            { key: 'nameTemplate', label: 'Nom', sortable: true, sortDirection: 'desc' },
            { key: 'fileTemplate', label: 'Contenu', sortable: false, sortDirection: 'desc' },
            { key: 'actions', label: 'Actions', sortable: false }
        ]
        return fields
    }
}