export default class Contact {

    static getFields () {
        let fields = [
            { key: 'lastnameContact', label: 'Nom', sortable: true, sortDirection: 'desc' },
            { key: 'firstnameContact', label: 'Prénom', sortable: true, sortDirection: 'desc' },
            { key: 'mailContact', label: 'Mail', sortable: true, sortDirection: 'desc' },
            { key: 'actions', label: 'Actions', sortable: false }
        ]
        return fields
    }
}