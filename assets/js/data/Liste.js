export default class Liste {

    static getFields () {
        let fields = [
            { key: 'nameListe', label: 'Nom', sortable: true, sortDirection: 'desc' },
            { key: 'actions', label: 'Actions', sortable: false }
        ]
        return fields
    }
}